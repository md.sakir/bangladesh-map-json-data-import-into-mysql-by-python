import json
import mysql.connector

# DB Connection
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="",
  database="socio_economy_new"
)

# DB Object
mycursor = mydb.cursor()

# Clear all records of bangladesh_maps table
mycursor.execute("DELETE FROM bangladesh_maps")
mydb.commit()
print(mycursor.rowcount, "record(s) deleted")

# Get json file
with open("bangladesh_geojson.json", "r") as f:
    data = json.load(f)

# Insert into DB
for record in data['features']:
    geometry = record["geometry"]
    geometry = json.dumps(geometry)
    sql = "INSERT INTO bangladesh_maps (division, district, upazila, geometry) VALUES (%s, %s, %s, %s)"
    val = (record["properties"]["ADM1_EN"], record["properties"]["ADM3_EN"], record["properties"]["ADM4_EN"], geometry)
    mycursor.execute(sql, val)
    mydb.commit()
    print("ADM0_EN : " + record["properties"]["ADM0_EN"]) #Country
    print("ADM1_EN : " + record["properties"]["ADM1_EN"]) #Division
    print("ADM2_EN : " + record["properties"]["ADM2_EN"])
    print("ADM3_EN : " + record["properties"]["ADM3_EN"]) #District
    print("ADM4_EN : " + record["properties"]["ADM4_EN"]) #Upazila
    print("***** Insertion done *****")

